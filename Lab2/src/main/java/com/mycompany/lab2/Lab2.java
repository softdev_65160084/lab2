/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

/**
 *
 * @author Siwak
 */
import java.util.Scanner;

public class Lab2 {
    int count = 0;
    static int row;
    static int col;
    static char currentPlayer = 'o';
    static char table[][] = new char[3][3];
    static Scanner kb = new Scanner(System.in);

    private static void createTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }
    }

    private static void displayBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println("\n-------------");
        }
    }

    private static void inputPlayer(char currentPlayer) {
        boolean correct = false;
        while (correct == false) {
            do {
                System.out.printf("Player " + currentPlayer + " : Please enter ur row, col: ");
                row = kb.nextInt() - 1;
                col = kb.nextInt() - 1;
            } while (row < 0 || col < 0 || row > 2 || col > 2);
            if (table[row][col] == '-') {
                table[row][col] = currentPlayer;
                correct = true;
            } else {
                System.out.println("Invalid move. Please try again.");
            }
        }
    }
    // private static void checkMove(char[][] table, char currentPlayer) {
    // boolean correct = false;
    // while (correct == false) {
    // if (table[row][col] == '-') {
    // table[row][col] = currentPlayer;
    // displayBoard();
    // correct = true;
    // } else {
    // System.out.println("Invalid move. Please try again.");
    // }
    // }
    // }

    private static void switchTurn() {
        if (currentPlayer == 'o') {
            currentPlayer = 'x';
        } else {
            currentPlayer = 'o';
        }
    }

    public static boolean checkWin(char[][] table, char currentPlayer) {
        for(row=0;row<3;row++){
            for(col=0;col<3;col++){
                if(table[row][0]==currentPlayer && table[row][1]==currentPlayer && table[row][2]==currentPlayer){
                    System.out.println("Player " + currentPlayer + " wins.");
                    return true;
                }
                if(table[0][col]==currentPlayer && table[1][col]==currentPlayer && table[2][col]==currentPlayer){
                    System.out.println("Player " + currentPlayer + " wins.");
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkDraw(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false; // If any empty cell is found, the table is not full
                }
            }
        }
        return true; // All cells are occupied, table is full
    }

    public static void main(String[] args) {
        createTable();
        displayBoard();
        while (checkWin(table, currentPlayer) == false && checkDraw(table) == false) {
            inputPlayer(currentPlayer);
            displayBoard();
            if(checkWin(table, currentPlayer) == true){
                break;
            }
            if(checkDraw(table) == true){
                break;
            }
            switchTurn();
        }
    }
}